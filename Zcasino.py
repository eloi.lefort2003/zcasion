#importation des bibliotheques
import math               
from math import ceil  
from random import randrange
import time 

#variable
nb_choisi = 0
nb_random = 0
argent_restant = 100
argent_mise = 0

#fonction
def gagant (): #si le joueur gagne il doit gagner ça 
    argent_restant == argent_restant + argent_mise * 3
    print("Bravo ! Vous avez gagne ! Vous remportez trois fois la mise ! Vous avez " + str(argent_restant) + " $.")

def argent_moitie (): #si le joueur tombe sur un mauvais numero mais si le nombre est aussi pair/impair
    math.ceil(argent_mise)
    argent_restant += argent_mise / 2
    print("Vous remportez la moitie de la somme que vous avez mise ! Il vous reste " + str(argent_restant) + " $.")

def perdant ():#si le joueur perd   
    argent_restant -= argent_mise
    print("Dommage ! Vous avez perdu, vous perdez integralement votre mise. Il vous reste " + str(argent_restant) + " $.")

    if argent_restant <= 0:
        print("Vous ne pouvez plus jouez car vous n'avez plus d'argent ! A bientot !")
  
#definir nombre sur lequel le joueur mise
nb_choisi = int(input("Bienvenue au casino ! Choisissez un nombre entre 1 et 49 ?"))

while nb_choisi < 1 or nb_choisi > 49:#si le joueur ne rentre pas un nombre entre 1 et 49
    int(input("Vous pouvez entrer un nombre seulement entre 1 et 49. Veuillez entrer un autre nombre."))
    break

#defnir la somme que veut miser le joueur
argent_mise = int(input("Vous pouvez depenser jusqu'a " + str(argent_restant) + "$ ! Quelle somme misez vous ?"))

while argent_mise > argent_restant:#si le joueur mise plus que son argent restant
    int(input("Vous n'avez pas assez d'argent pour miser ce nombre. Veuillez choisir une autre mise."))
    break

nb_random = randrange(50)
print("Le nombre de la roulette est " + str(nb_random))

if nb_choisi == nb_random:
    gagant()

elif nb_choisi % 2 == 0 and nb_random % 2 == 0:
    argent_moitie()

else:
    perdant()
